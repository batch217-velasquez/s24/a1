const num = 2;
const getCube = num ** 3;

console.log(`The cube of ${num} is ${getCube}`);
// 

const address = [258, "Washington Ave NW", "California", 90011 ];

const [ houseNumber, street, state, zipCode ] = address;

console.log(`I live at ${houseNumber} ${street}, ${state} ${zipCode}`);


// 
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in."
}

console.log(`${animal.name} was a ${animal.species}. He weighed at ${animal.weight} with a measurement of ${animal.measurement}`);



// 

const numbers = [1, 2, 3, 4, 5];

numbers.forEach((numbers) => {

	console.log(numbers);
})



const reduceNumber = numbers.reduce((previousValue, currentValue) => {
    return previousValue + currentValue
 })

 console.log(reduceNumber);



class Dog {
  constructor(name, age, breed) {
  	this.name = name,
  	this.age = age,
    this.breed = breed
    
    
  }
}

const myDog = new Dog('Frankie', 5, "Miniature Dachshund");
console.log(myDog);


